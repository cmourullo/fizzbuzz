<?php
class FizzBuzzCalculator
{
	private function isDivisibleBy($value, $divisor)
	{
		return ($value % $divisor) == 0;
	}

	public function calculate($input)
	{
		if ($input == 0)
			return 0;

		if ($this->isDivisibleBy($input, 3) && $this->isDivisibleBy($input, 5))
			return 'FizzBuzz';

		if ($this->isDivisibleBy($input, 3))
			return 'Fizz';

		if ($this->isDivisibleBy($input, 5))
			return 'Buzz';

		return $input;	
	}
}