<?php
require_once 'FizzBuzzCalculator.php';

class FizzBuzzCalculatorTest extends PHPUnit_Framework_TestCase
{
	protected function setUp()
    {
        $this->fizzBuzz = new FizzBuzzCalculator();
    }
    
    public function testShouldBeReturn0WhitInput0()
    {
        $this->assertEquals(0, $this->fizzBuzz->calculate(0));
    }

    public function testShouldBeReturn1WhitInput1()
    {
        $this->assertEquals(1, $this->fizzBuzz->calculate(1));
    }

    public function testShouldBeReturnFizzWhitInput3()
    {
        $this->assertEquals('Fizz', $this->fizzBuzz->calculate(3));
    }

    public function testShouldBeReturnFizzWhitInput6()
    {
        $this->assertEquals('Fizz', $this->fizzBuzz->calculate(6));
    }

    public function testShouldBeReturnBuzzWhitInput5()
    {
        $this->assertEquals('Buzz', $this->fizzBuzz->calculate(5));
    }

    public function testShouldBeReturnBuzzWhitInput10()
    {
        $this->assertEquals('Buzz', $this->fizzBuzz->calculate(10));
    }

    public function testShouldBeReturnBuzzWhitInput15()
    {
        $this->assertEquals('FizzBuzz', $this->fizzBuzz->calculate(15));
    }
}